import { Movie } from './movie';

export const MoviesList : Movie[] = [
    {id:1, year:1994, name:'The Shawshank Redemption'},
    {id:2, year:1972, name:'The Godfather'},
    {id:3, year:2008, name:'The Dark Knight'},
    {id:4, year:1993, name:'Schindlers List'},
    {id:5, year:1994, name:'Pulp Fiction'},
    {id:6, year:2003, name:'The Lord of the Rings'},
    {id:7, year:1966, name:'The Good, the Bad and the Ugly'},
    {id:8, year:1999, name:'Fight Club'},
    {id:9, year:1994, name:'Forrest Gump'},
    {id:10, year:2010, name:'Inception'},
]