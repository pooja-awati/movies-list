import { Component, OnInit } from '@angular/core';
import { Movie } from '../movie';

import { MoviesList } from '../movies-data';
@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  favoriteMovies = MoviesList;
  selectedMovie: Movie;
  onMovieSelect(movie: Movie): void {
    this.selectedMovie = movie;
  }
  constructor() { }

  ngOnInit() {
  }

}
